# This directory contains the various datasets for the class:

Two files exist for each dataset:

1. *.tar.xz - Compressed JPEG files. Files are organized into directories by class
1. *.ubyte.pkl.xz - Compressed Python pickled file for the pre-processed images.

The datasts are:

1. Aircraft/BIrds
   - AiBi

1. Armored Vehicles/Beaches/Oceans/Warships
   - ArBeOcWa

1. Automobiles/Ships/Trains
   - AuShTr
